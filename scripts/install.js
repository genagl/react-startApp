const fs = require('fs')
const child_process = require("child_process");

function config(){
    const directory_from = __dirname + "/template/config";
    const directory_to = __dirname + "/../src/config";


    fs.copyFileSync(  directory_from + "/config.json", directory_to +  "/config.json");
    fs.copyFileSync(  directory_from + "/layouts.json", directory_to +  "/layouts.json");
    fs.copyFileSync(  directory_from + "/ru-RU.json", directory_to +  "/ru-RU.json");
    fs.copyFileSync( directory_from + "/widgets.json",directory_to +  "/widgets.json");
    fs.writeFileSync( directory_to +  "/.gitignore", "config.json", 'utf8');
}



function clone(url, directory) {
    directory = __dirname + "/../src/" + directory
    if (!fs.existsSync(directory)) {
        child_process.execSync(`git clone ${url} "${directory}"`)
    }
}

function cloneWidget(url, directory) {
    directory = __dirname + "/../src/widgets/" + directory
    if (!fs.existsSync(directory)) {
        child_process.execSync(`git clone ${url} "${directory}"`)
    }
}

if (!fs.existsSync(__dirname + "/../src")){
    fs.mkdirSync(__dirname + "/../src");
}

if (!fs.existsSync(__dirname + "/../src/index.js")){
    const directory_from = __dirname + "/template";
    const directory_to = __dirname + "/../src";

    fs.copyFileSync( directory_from + "/index.js", directory_to +  "/index.js" );
}

if (!fs.existsSync(__dirname + "/../src/config")){
    fs.mkdirSync(__dirname + "/../src/config");
    config();
}

if (!fs.existsSync(__dirname + "/../src/widgets")){
    fs.mkdirSync(__dirname + "/../src/widgets");
}

clone("https://gitlab.com/protopiahome-public/protopia-ecosystem/libraries/layouts.git", "layouts");
clone("https://gitlab.com/protopiahome-public/protopia-ecosystem/libraries/layoutapp.git", "LayoutApp");

const widgets = require("../src/config/widgets.json");

if(widgets){
    widgets.forEach((e,i) => {
        if(e.url && e.name){
            cloneWidget(e.url, e.name);
        }
    })
}