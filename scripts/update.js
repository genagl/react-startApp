const fs = require('fs')
const child_process = require("child_process");

import modules from "../src/config/widgets.json";

function update(directory) {
    directory = __dirname + "/../src/" + directory
    if (fs.existsSync(directory)) {
        child_process.execSync(`cd ${directory} && git pull`)
    }
}

function updateWidget(directory) {
    directory = __dirname + "/../src/widgets/" + directory
    if (fs.existsSync(directory)) {
        child_process.execSync(`cd ${directory} && git pull`)
    }
}

update("layouts");
update("LayoutApp");

if(widgets){
    widgets.forEach((e,i) => {
        if(e.name){
            updateWidget(e.name);
        }
    })
}